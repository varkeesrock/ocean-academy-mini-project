select * from Bills

Create table User_Login (
             Id int identity(1,1) primary key not null,
			 Username varchar(20) not null,
			 UserPassword varchar(20) not null,
			 IsActive bit not null,
			 CreatedBy int  null,
			 UpdatedBy int  null,
			 CrearedDate DateTime not null,
			 UpdatedDate Datetime not null
)

Create Procedure usp_Employees (
			     @Id int,
				 @Name varchar(20),
				 @Password varchar(20),
				 @Age varchar(10),
				 @Gender varchar(20),
				 @Address varchar(50),
				 @Images varchar(100),
				 @Email varchar(20),
				 @Jobtype varchar(20),
				 @Salary decimal(18,2),
				 @ShiftTime varchar(20),
				 @Flag varchar(20) = ' '
)
as
Begin
     if @Flag = 'Insert'
	 Begin 
	  Insert into Employees (
	              [Name],
				  [Password],
				  Age,
				  Gender,
				  [Address],
				  Images,
				  Email,
				  Jobtype,
				  Salary,
				  ShiftTime
	        )
	  values (
	             @Name,
				 @Password,
				 @Age,
				 @Gender,
				 @Address,
				 @Images,
				 @Email,
				 @Jobtype,
				 @Salary,
				 @ShiftTime	        
	      )
	   end
	  IF @Flag = 'Update'
        BEGIN
            UPDATE Employees
            SET   [Name] = @Name,
				  [Password] = @Password,
				  Age = @Age,
				  Gender = @Gender,
				  [Address] = @Address,
				  Images = @Images,
				  Email = @Email,
				  Jobtype = @Jobtype,
				  Salary = @Salary,
				  ShiftTime = @ShiftTime
            WHERE  Id = @Id
        END
		ELSE IF @Flag = 'Delete'
        BEGIN
            DELETE FROM Employees
            WHERE  Id = @Id
        END
End

select * from Employees

--Insert the values using stored procedure Name in sql query
Exec usp_Employees @Id =1, @Name ='Robert', @Password ='Robert@123',@Age ='26',@Gender = 'Male',@Address ='yyyyyyy',@Images ='Photo of the Robert',@Email = 'yyyy@gggg',@Jobtype = 'Admin',@Salary = '50000',@ShiftTime = 'afternoon',@Flag = 'Insert'

--Update the values using stored procedure Name in sql query
Exec usp_Employees @Id =1,@Name ='Robert',@Password ='Robert@123',@Age ='26',@Gender = 'Male',@Address ='yyyyyyy',@Images ='Photo of the Robert',@Email = 'yyyy@gggg',@Jobtype = 'Admin',@Salary = '50000',@ShiftTime = 'afternoon',@Flag = 'Update'

Exec usp_Employees @Flag ='Delete',@Id =5;

Alter Procedure usp_Bills (
                 @Id int null,
				 @CustomerId int null,
				 @BookingId int null,
				 @Roomnumber varchar(20) null,
				 @Amount decimal(18,2) null,
				 @Flag varchar(20) = ' '
				 )
As
Begin
     if @Flag = 'Insert'
	 Begin 
	  Insert into Bills(
	               CustomerId,
				   BookingId,
				   Roomnumber,
				   Amount,
				   BillingDate      
	        )
	  values (
	             @CustomerId,
				 @BookingId,
				 @Roomnumber,
				 @Amount,
				 GETDATE()      
	      )
	   end
	    IF @Flag = 'Update'
        BEGIN
            UPDATE Bills
            SET  CustomerId = @CustomerId,
				 BookingId = @BookingId,
				 Roomnumber = @Roomnumber,
				 Amount = @Amount,
				 BillingDate = GETDATE()
            WHERE  Id = @Id
        END
		ELSE IF @Flag = 'Delete'
        BEGIN
            DELETE FROM Bills
            WHERE  Id = @Id
        END
End

select * from Bills

Exec usp_Bills @Id =2,@CustomerId = 4,@BookingId = 5,@Roomnumber ='200',@Amount = 3500.00,@Flag = 'Insert'

Exec usp_Bills @Id =1,@CustomerId = 2,@BookingId = 3,@Roomnumber ='450',@Amount = 4500.00,@Flag = 'Update'

Exec usp_Bills @Flag ='Delete',@Id =2;


Alter Procedure usp_Bookings (
                       @Id int,
					   @Roomnumber int = null,
					   @CustomerId int = null,
					   @CheckIn datetime = null,
					   @CheckOut datetime = null,
					   @Flag varchar(20) = ''
)
As
Begin 
     IF @Flag = 'Insert'
	 Begin
	 Insert into Bookings (
	             Roomnumber,
				 CustomerId,
				 CheckIn
	 )
	 values (
	        @Roomnumber,
			@CustomerId,
		    GETDATE()
	 )              
	 End
	  IF @Flag = 'Update'
	  Begin
	     Update Bookings
		 Set  CheckOut = GETDATE()
		 where Id = @Id
	  End
End

select * from Bookings 
order by Id

Exec usp_Bookings  @Id =4, @Roomnumber = 300, @CustomerId = 7 , @Flag = 'Insert' 

Exec usp_Bookings @Id =4,@Flag='Update'

--Alter column datatype , null or not null 
Alter Table Bookings 
alter column CheckOut datetime null

--Change table Name
EXEC sp_rename 'User_Login', 'User_Logins';

--Change Column Name 
EXEC sp_rename 'User_Logins.CrearedDate', 'CreateDate', 'COLUMN';

--Stored Procedure for Customers
create Procedure usp_Customers (
						@Id int,
						@Name varchar(20),
						@Address varchar(150),
						@Mobile varchar(20),
						@Gender varchar(20),
						@Images varchar(100),
						@Email varchar(30),
						@Flag varchar(20) = ''
						)
As
Begin
   If @Flag = 'Insert'
   begin
   Insert into Customers (
					[Name],
					[Address],
					Mobile,
					Gender,
					Images,
					Email
   )
           Values (
				  @Name,
				  @Address,
				  @Mobile,
			      @Gender,
				  @Images,
				  @Email
   )
   End
   If @Flag ='Update'
   begin
   Update Customers
   set				[Name] = @Name,
					[Address] = @Address,
					Mobile = @Mobile,
					Gender = @Gender,
					Images = @Images,
					Email = @Email
   end
   ELSE IF @Flag = 'Delete'
        BEGIN
            DELETE FROM Customers
            WHERE  Id = @Id
        END
End

select * from Customers

Exec usp_Customers  @Id =1,@Name ='Sam',@Address = 'zzzzz',@Mobile ='87821211',@Gender ='male',@Images = 'Photo of Sam',@Email = 'sss@gmail.com', @Flag = 'Insert' 

Exec usp_Customers  @Id =1,@Name ='Sam',@Address = 'yyyyyyy',@Mobile ='87821211',@Gender ='male',@Images = 'Photo of Sam',@Email = 'sss@gmail.com', @Flag = 'Update' 



DBCC CHECKIDENT (Bookings, RESEED, 1011);

insert into Bookings (Roomnumber,CustomerId,CheckIn,CheckOut)
values(250,7,GETDATE(),GETDATE())

select * from Employees

select * from Bookings


Alter Procedure usp_BookingsList(
                   @Flag varchar(20) = ''
)
as
Begin
 If @Flag ='SelectAll'
	begin	
     Select Bookings.Id as[Bookingvm.Id],
	 Customers.[Name]as[Name],
	 Customers.[Address]as[Address],
	 Customers.Email as [Email],
	 Customers.Mobile as [Mobile],
	 Customers.Gender as [Gender],
	 Bookings.Roomnumber as [Bookingvm.Roomnumber],
	 Bookings.CheckIn as [Bookingvm.CheckIn],
	 Bookings.CheckOut as [Bookingvm.CheckOut] 
     from Bookings
     Inner JOIN  Customers
     on
     Customers.Id = Bookings.CustomerId
	 order by Bookings.Id
	end
End

Exec usp_BookingsList @Flag='SelectAll'

select * from Customers

DELETE TOP (5) PERCENT
FROM Customers;

insert into Rooms(Roomnumber,[Description],DayCharge,[status])
values(103,' A/c',1000,'available')

select * from Rooms