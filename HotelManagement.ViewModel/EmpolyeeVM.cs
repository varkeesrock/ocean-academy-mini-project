﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HotelManagement.Model;

namespace HotelManagement.ViewModel
{
    public class EmpolyeeVM
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Age { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Images { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Jobtype { get; set; }
        public decimal Salary { get; set; }
        public string ShiftTime { get; set; }
        public List<EmpolyeeVM> EmpolyeeVMlist { get; set; }

        public Employee ToModel(EmpolyeeVM _objEmployeeVM)
        {
            Employee _objEmployee = new Employee();

            _objEmployee.Id = _objEmployeeVM.Id;
            _objEmployee.Name = _objEmployeeVM.Name;
            _objEmployee.Password = _objEmployeeVM.Password;
            _objEmployee.Age = _objEmployeeVM.Age;
            _objEmployee.Gender = _objEmployeeVM.Gender;
            _objEmployee.Address = _objEmployeeVM.Address;
            _objEmployee.Images = _objEmployeeVM.Images;
            _objEmployee.Email = _objEmployeeVM.Email;
            _objEmployee.Salary = _objEmployeeVM.Salary;
            _objEmployee.ShiftTime = _objEmployeeVM.ShiftTime;

            return _objEmployee;

        }
        public EmpolyeeVM ToViewModel(Employee _objEmployee)
        {
            EmpolyeeVM _objEmployeeVM = new EmpolyeeVM();

            _objEmployeeVM.Id = _objEmployee.Id;
            _objEmployeeVM.Name = _objEmployee.Name;
            _objEmployeeVM.Password = _objEmployee.Password;
            _objEmployeeVM.Age = _objEmployee.Age;
            _objEmployeeVM.Gender = _objEmployee.Gender;
            _objEmployeeVM.Address = _objEmployee.Address;
            _objEmployeeVM.Images = _objEmployee.Images;
            _objEmployeeVM.Email = _objEmployee.Email;
            _objEmployeeVM.Salary = _objEmployee.Salary;
            _objEmployeeVM.ShiftTime = _objEmployee.ShiftTime;

            return _objEmployeeVM;

        }
    }
}
