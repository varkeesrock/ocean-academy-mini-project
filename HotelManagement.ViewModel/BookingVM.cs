﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HotelManagement.Model;

namespace HotelManagement.ViewModel
{
    public class BookingVM
    {
        public int Id { get; set; }
        [Required]
        public int Roomnumber { get; set; }
        public int CustomerId { get; set; }
        [Required]
        public DateTime CheckIn { get; set; }
        public DateTime? CheckOut { get; set; } 
        public bool Action { get; set; }

        public Booking ToModel(BookingVM _objbookingVM)
        {
            Booking _objBooking = new Booking();

            _objBooking.CustomerId = _objbookingVM.CustomerId;
            _objBooking.Id = _objbookingVM.Id;
            _objBooking.Roomnumber = _objbookingVM.Roomnumber;
            _objBooking.CheckIn = _objbookingVM.CheckIn;
            _objBooking.CheckOut = _objbookingVM.CheckOut;
            return _objBooking; 

        }
        public BookingVM ToViewModel(Booking _objbooking)
        {
            BookingVM _objBookingVM = new BookingVM();

            _objBookingVM.CustomerId = _objbooking.CustomerId;
            _objBookingVM.Id = _objbooking.Id;
            _objBookingVM.Roomnumber = _objbooking.Roomnumber;
            _objBookingVM.CheckIn = _objbooking.CheckIn;

            return _objBookingVM; 

        }

          }


}
