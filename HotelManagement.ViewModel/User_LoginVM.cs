﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HotelManagement.ViewModel
{
    public class User_LoginVM
    {
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string UserPassword { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public int UpdateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
