﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HotelManagement.ViewModel
{
    public class RoomVM
    {
        public int Id { get; set; }
        public int Roomnumber { get; set; }
        public string Description { get; set; }
        public int DayCharge { get; set; }
        public string status { get; set; }
        public List<RoomVM> RoomVMlist { get; set; }
    }
}
