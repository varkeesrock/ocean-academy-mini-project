﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HotelManagement.Model;
using System.Web;

namespace HotelManagement.ViewModel
{
    public class CustomerVM
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Mobile { get; set; }
        public string Gender { get; set; }
        public string Images { get; set; }
        [Required]
        public string Email { get; set; }
        public List<CustomerVM> customerVMlist { get; set; }

        public BookingVM Bookingvm { get; set; }


        public Customer ToModel(CustomerVM _objCustomerVM)
        {
            Customer _objCustomer = new Customer();

            _objCustomer.Id = _objCustomerVM.Id;
            _objCustomer.Name = _objCustomerVM.Name;
            _objCustomer.Address = _objCustomerVM.Address;
            _objCustomer.Mobile = _objCustomerVM.Mobile;
            _objCustomer.Gender = _objCustomerVM.Gender;
            _objCustomer.Images = _objCustomerVM.Images;
            _objCustomer.Email = _objCustomerVM.Email;

            return _objCustomer;
        }
        public CustomerVM ToViewModel(Customer _objCustomer)
        {
            CustomerVM _objCustomerVM = new CustomerVM();

            _objCustomerVM.Id = _objCustomer.Id;
            _objCustomerVM.Name = _objCustomer.Name;
            _objCustomerVM.Address = _objCustomer.Address;
            _objCustomerVM.Mobile = _objCustomer.Mobile;
            _objCustomerVM.Gender = _objCustomer.Gender;
            _objCustomerVM.Images = _objCustomer.Images;
            _objCustomerVM.Email = _objCustomer.Email;

            return _objCustomerVM;
        }
    }
}
