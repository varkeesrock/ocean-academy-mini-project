﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HotelManagement.ViewModel
{
    public class LoginAuditVM
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string Loginstatus { get; set; }
        public DateTime LastLogintime { get; set; }
    }
}
