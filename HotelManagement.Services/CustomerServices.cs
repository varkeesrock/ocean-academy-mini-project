﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelManagement.ViewModel;
using HotelManagement.Business;

namespace HotelManagement.Services
{
   public class CustomerServices
    {
        public List<CustomerVM> GetBookingsList()
        {
            try
            {
                List<CustomerVM> _objCustomerBLList = new List<CustomerVM>();
                CustomerBL _objCustomerBL = new CustomerBL();

                _objCustomerBLList = _objCustomerBL.GetBookingsList();

                return _objCustomerBLList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<CustomerVM> GetCustomerList()
        {
            try
            {
                List<CustomerVM> _objCustomerBLList = new List<CustomerVM>();
                CustomerBL _objCustomerBL = new CustomerBL();

                _objCustomerBLList = _objCustomerBL.GetCustomerList();

                return _objCustomerBLList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public CustomerVM GetCustomerById(int Id)
        //{
        //    try
        //    {
        //        CustomerVM _objCustomerVM = new CustomerVM();
        //        CustomerBL _objCustomerBL = new CustomerBL();

        //        _objCustomerVM = _objCustomerBL.GetCustomerById(Id);

        //        return (_objCustomerVM);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public CustomerVM create(CustomerVM _objCustomerVM)
        {
            try
            {
                BookingBL _objBookungBL = new BookingBL();

                return _objBookungBL.create(_objCustomerVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CustomerVM Update(CustomerVM _objCustomerVM)
        {
            try
            {
                BookingBL _objCustomerBL = new BookingBL();

                return _objCustomerBL.Update(_objCustomerVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public bool delete(int _id)
        //{
        //    try
        //    {
        //        CustomerVM _objCustomerVM = new CustomerVM();
        //        CustomerBL _objCustomerBL = new CustomerBL();

        //        return _objCustomerBL.Delete(_id);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
