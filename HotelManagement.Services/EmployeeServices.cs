﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelManagement.ViewModel;
using HotelManagement.Business;

namespace HotelManagement.Services
{
    public class EmployeeServices
    {
        public List<RoomVM> GetRoomList()
        {
            try
            {
                List<RoomVM> _objRoomVMList = new List<RoomVM>();
                CustomerBL _objCustomerBL = new CustomerBL();

                _objRoomVMList = _objCustomerBL.GetRoomList();

                return _objRoomVMList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<EmpolyeeVM> GetEmployeeList()
        {
            try
            {
                List<EmpolyeeVM> _objEmployeeVMList = new List<EmpolyeeVM>();
                CustomerBL _objCustomerBL = new CustomerBL();

                _objEmployeeVMList = _objCustomerBL.GetEmployeeList();

                return _objEmployeeVMList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CustomerVM GetEmployeeById(int Id)
        {
            try
            {
                CustomerVM _objcustomerVM = new CustomerVM();
                CustomerBL _objCustomerBL = new CustomerBL();

                _objcustomerVM = _objCustomerBL.GetEmployeeById(Id);

                return _objcustomerVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
