﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Model
{
    public class LoginAudit
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string Loginstatus { get; set; }
        public DateTime LastLogintime { get; set; }
    }
}
