﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Model
{
    public class Room
    {
        public int Id { get; set; }
        public int Roomnumber { get; set; }
        public string Description { get; set; }
        public int DayCharge { get; set; }
        public string status { get; set; }
    }
}
