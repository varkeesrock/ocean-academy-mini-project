﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Model
{
    public class Bill
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int BookingId { get; set; }
        public string Roomnumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime BillingDate { get; set; }
    }
}
