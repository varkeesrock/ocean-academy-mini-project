﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Model
{
    public class User_Login
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string UserPassword { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public int UpdateBy { get; set; }
        public  DateTime CreateDate { get; set; }
        public  DateTime UpdatedDate { get; set; }
    }
}
