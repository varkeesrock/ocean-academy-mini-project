﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Images { get; set; }
        public string Email { get; set; }
        public string Jobtype { get; set; }
        public decimal Salary { get; set; }
        public string ShiftTime { get; set; }
        public List<Employee> Employeeslist { get; set; }
    }
}
