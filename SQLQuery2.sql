
 Select Bookings.Id as[Bookingvm.Id],
	 Customers.[Name]as[Name],
	 Customers.[Address]as[Address],
	 Customers.Email as [Email],
	 Customers.Mobile as [Mobile],
	 Customers.Gender as [Gender],
	 Bookings.Roomnumber as [Bookingvm.Roomnumber],
	 Bookings.CheckIn as [Bookingvm.CheckIn],
	 Bookings.CheckOut as [Bookingvm.CheckOut] 
     from Bookings
      JOIN  Customers
     on
     Customers.Id = Bookings.CustomerId
--where Bookings.Id=1010
order by Bookings.Id
