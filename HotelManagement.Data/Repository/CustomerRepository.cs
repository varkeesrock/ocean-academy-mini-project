﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelManagement.Model;
using HotelManagement.ViewModel;
using System.Data.SqlClient;

namespace HotelManagement.Data.Repository
{
    public class CustomerRepository
    {
        public List<CustomerVM> GetBookingsList()
        {
            try
            {
                using (HotelManagementDBContext EmpDBContext = new HotelManagementDBContext())
                {
                    List<CustomerVM> _objBookingsVMList = new List<CustomerVM>();
                    CustomerVM _objCustomerVM = new CustomerVM();
                    BookingVM _objBookingVM  = new BookingVM();

                    _objBookingsVMList = EmpDBContext.Customers.Join(EmpDBContext.Bookings, a => a.Id, b => b.CustomerId, (a, b) => new CustomerVM()
                    {
                        Name = a.Name,
                        Address = a.Address,
                        Email = a.Email,
                        Mobile = a.Mobile,
                        Gender = a.Gender,
                        Images =a.Images,
                        Bookingvm = new BookingVM()
                        {
                            Id = b.Id,
                            Roomnumber = b.Roomnumber,
                            CheckIn = b.CheckIn,
                            CheckOut = b.CheckOut
                        }

                    }).ToList();

                    return _objBookingsVMList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }  
        public CustomerVM GetBookingById(int Id)
        {
            try
            {
                using (HotelManagementDBContext EmpDBContext = new HotelManagementDBContext())
                {
                    List<CustomerVM> _objBookingsVMList = new List<CustomerVM>();
                    CustomerVM _objCustomerVM = new CustomerVM();
                    BookingVM _objBookingVM  = new BookingVM();

                    _objCustomerVM = EmpDBContext.Customers.Join(EmpDBContext.Bookings.Where(t => t.Id == Id), a => a.Id, b => b.CustomerId, (a, b) => new CustomerVM()
                    {
                        Name = a.Name,
                        Address = a.Address,
                        Email = a.Email,
                        Mobile = a.Mobile,
                        Gender = a.Gender,
                        Images = a.Images,
                        Bookingvm = new BookingVM()
                        {
                            Id = b.Id,
                            Roomnumber = b.Roomnumber,
                            CheckIn = b.CheckIn,
                            CheckOut = b.CheckOut
                        }

                    }).SingleOrDefault();

                    return _objCustomerVM;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        public List<CustomerVM> GetCustomerList()
        {
            try
            {
                using (HotelManagementDBContext EmpDBContext = new HotelManagementDBContext())
                {
                    List<CustomerVM> _objCustomerList = new List<CustomerVM>();

                    _objCustomerList = EmpDBContext.Customers.Select(y => new CustomerVM()
                    {
                        Id = y.Id,
                        Name = y.Name,
                        Address = y.Address,
                        Mobile = y.Mobile,
                        Gender = y.Gender,
                        Email = y.Email,
                        Images =y.Images
                    }).ToList();

                    return _objCustomerList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        public List<RoomVM> GetRoomList()
        {
            try
            {
                using (HotelManagementDBContext EmpDBContext = new HotelManagementDBContext())
                {
                    List<RoomVM> _objRoomList = new List<RoomVM>();

                    _objRoomList = EmpDBContext.Rooms.Select(y => new RoomVM()
                    {
                       Id = y.Id,
                       Roomnumber = y.Roomnumber,
                       Description = y.Description,
                       DayCharge = y.DayCharge,
                       status= y.status
                    }).ToList();

                    return _objRoomList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 


       
    }
}
