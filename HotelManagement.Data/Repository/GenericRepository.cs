﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HotelManagement.Data.Repository
{
    public class GenericRepository<T> where T : class
    {
        public HotelManagementDBContext EmpDBContext = null;
        private DbSet<T> table = null;


        #region Constructor

        public GenericRepository()
        {
            this.EmpDBContext = new HotelManagementDBContext();
            table = EmpDBContext.Set<T>();
        }

        public GenericRepository(HotelManagementDBContext _objEmpDBContext)
        {
            this.EmpDBContext = _objEmpDBContext;
            table = _objEmpDBContext.Set<T>();
        }
        #endregion

        public void Dispose()
        {
            this.EmpDBContext.Dispose();
        }


        public List<T> All()
        {
            return table.ToList();
        }


        public T SelectById(object id)
        {
            return table.Find(id);
        }


        public T SelectById(int id)
        {
            return table.Find(id);
        }

        public T Add(T obj)
        {
            try
            {
                return table.Add(obj);
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }


        public T Update(T obj)
        {
            try
            {
                var result = table.Attach(obj);
                EmpDBContext.Entry(obj).State = EntityState.Modified;
                return result;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }

        public void Delete(object id)
        {
            try
            {
                T existing = table.Find(id);
                table.Remove(existing);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Save()
        {

            EmpDBContext.SaveChanges();
        }

        public IQueryable<T> Where(Func<T, bool> expression)
        {
            return table.Where(expression).AsQueryable();
        }

    }
}
