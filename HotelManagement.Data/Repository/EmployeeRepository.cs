﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using HotelManagement.Model;
using HotelManagement.ViewModel;

namespace HotelManagement.Data.Repository
{
   public class EmployeeRepository
    {
        public List<EmpolyeeVM> GetEmployeeList()
        {
            try
            {
                using (HotelManagementDBContext EmpDBContext = new HotelManagementDBContext())
                {
                    List<EmpolyeeVM> _objEmployeeVMList = new List<EmpolyeeVM>();

                    _objEmployeeVMList = EmpDBContext.Employees.Select(y => new EmpolyeeVM()
                    {
                        Id = y.Id,
                        Name = y.Name,
                        Password = y.Password,
                        Age = y.Age,
                        Gender = y.Gender,
                        Address = y.Address,
                        Images = y.Images,
                        Email = y.Email,
                        Jobtype = y.Jobtype,
                        Salary = y.Salary,
                        ShiftTime = y.ShiftTime

                    }).ToList();

                    return _objEmployeeVMList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
