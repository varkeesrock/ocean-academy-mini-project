﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelManagement.ViewModel;
using HotelManagement.Data.Repository;
using HotelManagement.Model;

namespace HotelManagement.Business
{
    public class CustomerBL : GenericBL<Customer>
    {
        public List<CustomerVM> GetBookingsList()
        {
            try
            {
                CustomerVM _objCustomerVM = new CustomerVM();
                List<CustomerVM> _objCustomerVMList = new List<CustomerVM>();
                 CustomerRepository _objCustomRepo = new CustomerRepository();

                 _objCustomerVMList = _objCustomRepo.GetBookingsList();


                return _objCustomerVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }  
        public List<CustomerVM> GetCustomerList()
        {
            try
            {
                CustomerVM _objCustomerVM = new CustomerVM();
                List<CustomerVM> _objCustomerVMList = new List<CustomerVM>();
                 CustomerRepository _objCustomRepo = new CustomerRepository();

                 _objCustomerVMList = _objCustomRepo.GetCustomerList();


                return _objCustomerVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        } 
        
        public List<RoomVM> GetRoomList()
        {
            try
            {
                RoomVM _objRoomVM = new RoomVM();
                List<RoomVM> _objRoomVMList = new List<RoomVM>();
                 CustomerRepository _objCustomRepo = new CustomerRepository();

                _objRoomVMList = _objCustomRepo.GetRoomList();


                return _objRoomVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<EmpolyeeVM> GetEmployeeList()
        {
            try
            {
                EmpolyeeVM _objCustomerVM = new EmpolyeeVM();
                List<EmpolyeeVM> _objEmployeeVMList = new List<EmpolyeeVM>();
                EmployeeRepository _objEmployeeRepo = new EmployeeRepository();

                _objEmployeeVMList = _objEmployeeRepo.GetEmployeeList();


                return _objEmployeeVMList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public CustomerVM GetEmployeeById(int Id)
        {
            try
            {
                CustomerVM _objCustomerVM = new CustomerVM();
                BookingVM _objbookingVM = new BookingVM();
                CustomerRepository _objCustRepo = new CustomerRepository();

                _objCustomerVM = _objCustRepo.GetBookingById(Id);
               // _objbooking = _objEmpBL.SelectById(Id);
                return _objCustomerVM;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //public bool Delete(int _id)
        //{
        //    CustomerRepository _objCustomRepo = new CustomerRepository();

        //    return _objCustomRepo.Delete(_id);
        //}
    }
}
