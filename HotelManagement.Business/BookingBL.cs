﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelManagement.ViewModel;
using HotelManagement.Data.Repository;
using HotelManagement.Model;

namespace HotelManagement.Business
{
    public class BookingBL : GenericBL<Booking>
    {
        public CustomerVM create(CustomerVM _objCustomerVM)
        {
            Customer _objCustomer = new Customer();
            BookingVM _objbookingVM = new BookingVM();
            Booking _ObjBooking = new Booking();
            try
            {

            CustomerBL _objCustomerBL = new CustomerBL();
            BookingBL _objbookingBL = new BookingBL();

            _objCustomer = _objCustomerVM.ToModel(_objCustomerVM);
            _objCustomer = _objCustomerBL.Add(_objCustomer);
                _objCustomerVM.Id = _objCustomer.Id;
                _objCustomerVM.Bookingvm.CustomerId = _objCustomer.Id;
            _ObjBooking = _objbookingVM.ToModel(_objCustomerVM.Bookingvm);
            _ObjBooking = _objbookingBL.Add(_ObjBooking);

                _objCustomerVM.Bookingvm.Id = _ObjBooking.Id;

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return (_objCustomerVM);
        }

        public CustomerVM Update(CustomerVM _objCustomerVM)
        {
            try
            {
                Customer _objCustomer = new Customer();
                BookingVM _objbookingVM = new BookingVM();
                Booking _ObjBooking = new Booking();

                CustomerBL _objCustomerBL = new CustomerBL();
                BookingBL _objbookingBL = new BookingBL();

                //_objCustomer = _objCustomerVM.ToModel(_objCustomerVM);
                //_objCustomer = _objCustomerBL.Update(_objCustomer);
                //_objCustomerVM.Bookingvm.CustomerId = _objCustomer.Id;
                _ObjBooking = _objbookingVM.ToModel(_objCustomerVM.Bookingvm);
                _ObjBooking = _objbookingBL.Update(_ObjBooking);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return (_objCustomerVM);
        }

    }
}
