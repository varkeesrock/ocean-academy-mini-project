﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using HotelManagement.Data;
using HotelManagement.Data.Repository;

namespace HotelManagement.Business
{
    public class GenericBL<T> where T : class
    {
        public GenericRepository<T> _objRepo;
        public HotelManagementDBContext DBContext;
        public GenericBL()
        {
            DBContext = new HotelManagementDBContext();
            _objRepo = new GenericRepository<T>(DBContext);
        }
        public GenericBL(HotelManagementDBContext _DBContext)
        {
            DBContext = _DBContext;
            _objRepo = new GenericRepository<T>(_DBContext);
        }



        public T Add(T List)
        {
            T obj = _objRepo.Add(List);
            _objRepo.Save();

            return obj;
        }



        public List<T> SelectAll()
        {
            return _objRepo.All();
        }

        public T SelectById(object Id)
        {
            return _objRepo.SelectById(Id);
        }

        public void InsertAll(IEnumerable<T> List)
        {

            foreach (var item in List)
            {
                _objRepo.Add(item);
                _objRepo.Save();
            }

        }



        public void Delete(object _obj)
        {
            _objRepo.Delete(_obj);
            _objRepo.Save();
        }


        public T Update(T _obj)
        {

            T obj = _objRepo.Update(_obj);
            _objRepo.Save();
            return obj;
        }

        public void UpdateAll(IEnumerable<T> List)
        {

            foreach (var item in List)
            {
                _objRepo.Update(item);
                _objRepo.Save();
            }

        }
    }
}
