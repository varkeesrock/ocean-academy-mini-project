﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HotelManagement.ViewModel;
using HotelManagement.Services;

namespace HotelManagement.Controllers
{
    public class InformationController : Controller
    {
        // GET: Information
        [HttpGet]
        public ActionResult CustomerInfo()
        {
            try
            {
                CustomerServices _objcustomerServices = new CustomerServices();
                CustomerVM _objcustomerVM = new CustomerVM();
                _objcustomerVM.customerVMlist = new List<CustomerVM>();

                _objcustomerVM.customerVMlist = _objcustomerServices.GetCustomerList();


                return View(_objcustomerVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        [HttpGet]
        public ActionResult RoomInfo()
        {
            try
            {
                EmployeeServices _objEmployeeServices = new EmployeeServices();
                RoomVM _objRoomVM = new RoomVM();
                _objRoomVM.RoomVMlist = new List<RoomVM>();

                _objRoomVM.RoomVMlist = _objEmployeeServices.GetRoomList();


                return View(_objRoomVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        [HttpGet]
        public ActionResult EmployeeInfo()
        {
            try
            {

                EmployeeServices _objEmployeeServices = new EmployeeServices();
                EmpolyeeVM _objEmployeeVM = new EmpolyeeVM();
                _objEmployeeVM.EmpolyeeVMlist = new List<EmpolyeeVM>();

                _objEmployeeVM.EmpolyeeVMlist = _objEmployeeServices.GetEmployeeList();


                return View(_objEmployeeVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}