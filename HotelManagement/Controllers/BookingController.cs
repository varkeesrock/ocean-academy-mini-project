﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HotelManagement.Services;
using HotelManagement.ViewModel;

namespace HotelManagement.Controllers
{
    public class BookingController : Controller
    {
        // GET: Booking
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                CustomerVM _objcustomerVMList = new CustomerVM();
                _objcustomerVMList.customerVMlist = new List<CustomerVM>();
                CustomerServices _objCustomerServices = new CustomerServices();

                _objcustomerVMList.customerVMlist = _objCustomerServices.GetBookingsList();

                return View(_objcustomerVMList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]

        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [HttpPost]
        public ActionResult Create(CustomerVM _objCustomerVM)
        {
            try
            {

                CustomerServices _objCustomerServices = new CustomerServices();
                _objCustomerServices.create(_objCustomerVM);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            try
            {
                CustomerVM _objcustomerVM = new CustomerVM();
                _objcustomerVM.customerVMlist = new List<CustomerVM>();

                EmployeeServices _objEmployeeServices = new EmployeeServices();

                _objcustomerVM = _objEmployeeServices.GetEmployeeById(Id);

                return View(_objcustomerVM);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [HttpPost]

        public ActionResult Update(CustomerVM _objCustomerVM)
        {
            try
            {
                CustomerServices _objCostomerServices = new CustomerServices();
                _objCostomerServices.Update(_objCustomerVM);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}