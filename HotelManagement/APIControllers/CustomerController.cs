﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HotelManagement.Services;
using HotelManagement.ViewModel;

namespace HotelManagement.APIControllers
{
    [RoutePrefix("api/Customer")]
    public class CustomerController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetCustomer()
        {
            CustomerServices _objCostomerServices = new CustomerServices();
            CustomerVM _objCustomerVM = new CustomerVM();
            _objCustomerVM.customerVMlist = new List<CustomerVM>();

            _objCustomerVM.customerVMlist = _objCostomerServices.GetCustomerList();


            return Ok(_objCustomerVM.customerVMlist);
        }

        //[HttpGet]
        //[Route("{id}")]
        //public IHttpActionResult GetCustomerById(int id)
        //{
        //    CustomerServices _objCostomerServices = new CustomerServices();
        //    CustomerVM _objCustomerVM = new CustomerVM();

        //    _objCustomerVM = _objCostomerServices.GetCustomerById(id);


        //    return Ok(_objCustomerVM);
        //}
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(CustomerVM _objCustomerVM)
        {
            try
            {
                CustomerServices _objCostomerServices = new CustomerServices();
                //_objCustomerVM.BookingVM  = new BookingVM();
                _objCostomerServices.create(_objCustomerVM);
                return Ok(_objCustomerVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(CustomerVM _objCustomerVM)
        {
            try
            {
                CustomerServices _objCostomerServices = new CustomerServices();
                _objCostomerServices.Update(_objCustomerVM);
                return Ok(_objCustomerVM);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //[HttpDelete]
        //[Route("{id}")]
        //public IHttpActionResult Delete(int Id)
        //{
        //    try
        //    {
        //        CustomerServices _objCostomerServices = new CustomerServices();
        //        var result = _objCostomerServices.delete(Id);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

    }
}
